import React, { Component } from "react";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import RaisedButton from "material-ui/RaisedButton";
import Appbar from "material-ui/AppBar";
import List from "material-ui/List";
import Divide from "material-ui/Divider";

import Player from "./Player";
import TeamList from "./Team-list";
import TeamStats from "./Team-stats";

export default class App extends Component {
  getPlayers() {
    return [
      {
        _id: 1,
        name: "motmi",
        ballmanip: 2,
        kicking: 3,
        passing: 1,
        duel: 1,
        fieldCover: 3,
        blocking: 2,
        gameStrang: 3,
        playMaking: 2
      },
      {
        _id: 2,
        name: "ronaldo",
        ballmanip: 2,
        kicking: 3,
        passing: 1,
        duel: 1,
        fieldCover: 3,
        blocking: 2,
        gameStrang: 3,
        playMaking: 2
      },
      {
        _id: 3,
        name: "messi",
        ballmanip: 2,
        kicking: 3,
        passing: 1,
        duel: 1,
        fieldCover: 3,
        blocking: 2,
        gameStrang: 3,
        playMaking: 2
      }
    ];
  }

  renderPlayers() {
    return this.getPlayers().map(Player => (
      <TeamList key={Player._id} player={Player} />
    ));
  }
  render() {
    return (
      <MuiThemeProvider>
        <div className="container">
          <Appbar
            title="Soccer Game App"
            iconClassNameRight="muidocs-icon"
            showMenuIconButton={false}
          />
          <div className="row">
            <div className="col s12 m7">
              <Player />
            </div>
            <div className="col s12 m5">
              <Divide />
              <List>{this.renderPlayers()}</List>
            </div>
            <div className="col s12 m5">
              <TeamStats />
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}
