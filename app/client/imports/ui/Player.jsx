import React, { Component } from "react";
import {
  Card,
  CardActions,
  CardHeader,
  CardMedia,
  CardText,
  CardTitle
} from "material-ui/Card";
import Avatar from "material-ui/Avatar";
import Chip from "material-ui/Chip";
import { blue200, blue900 } from "material-ui/styles/colors";

const styles = {
  chip: {
    margin: 4
  },
  wrapper: {
    display: "flex",
    flexWrap: "wrap"
  },
  button: {
    margin: 12
  }
};
export default class Player extends Component {
  render() {
    return (
      <Card>
        <CardMedia
          overlay={<CardTitle title="Card title" subtitle="Card subtitle" />}
        >
          <img src="football.jpg" alt="football" />
        </CardMedia>
        <CardText>
          <div style={styles.wrapper}>
            <Chip backgroundColor={blue200} style={styles.chip}>
              <Avatar size={32} color={blue200} backgroundColor={blue900}>
                PL
              </Avatar>
              football player
            </Chip>
            <Chip backgroundColor={blue200} style={styles.chip}>
              <Avatar size={32} color={blue200} backgroundColor={blue900}>
                PL
              </Avatar>
              player
            </Chip>
            <Chip backgroundColor={blue200} style={styles.chip}>
              <Avatar size={32} color={blue200} backgroundColor={blue900}>
                PL
              </Avatar>
              football player
            </Chip>
            <Chip backgroundColor={blue200} style={styles.chip}>
              <Avatar size={32} color={blue200} backgroundColor={blue900}>
                PL
              </Avatar>
              football
            </Chip>
            <Chip backgroundColor={blue200} style={styles.chip}>
              <Avatar size={32} color={blue200} backgroundColor={blue900}>
                PL
              </Avatar>
              football player
            </Chip>
            <Chip backgroundColor={blue200} style={styles.chip}>
              <Avatar size={32} color={blue200} backgroundColor={blue900}>
                PL
              </Avatar>
              goal kepper player
            </Chip>
            <Chip backgroundColor={blue200} style={styles.chip}>
              <Avatar size={32} color={blue200} backgroundColor={blue900}>
                PL
              </Avatar>
              football
            </Chip>
            <Chip backgroundColor={blue200} style={styles.chip}>
              <Avatar size={32} color={blue200} backgroundColor={blue900}>
                MB
              </Avatar>
              player
            </Chip>
          </div>
        </CardText>
        <CardActions />
      </Card>
    );
  }
}
